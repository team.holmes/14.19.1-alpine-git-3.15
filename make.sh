#!/bin/bash
# https://hub.docker.com/repository/docker/teamholmes/node
# docker pull teamholmes/node:14.19.1-alpine-git-3.15
# https://gitlab.com/team.holmes/14.19.1-alpine-git-3.15
docker build -t teamholmes/node:14.19.1-alpine-git-3.15 .
docker push teamholmes/node:14.19.1-alpine-git-3.15